# Pipeline wait

This project is a simple demo on how you could make a pipeline wait until a specific date / time.

## How to use

1. Add a `wait` stage to your `.gitlab-ci.yml` file
2. Include the `Wait.gitlab-ci.yml`
3. Configure a variable called `WAIT_UNTIL` to be used by the script

## How this works

The script reads the value in the `WAIT_UNTIL` variable and converts it to seconds using `date`; it then invokes `sleep` to wait until the specified time. The stage runs with base alpine image.

Make sure you are passing the date with the correct timezone, this will be the timezone.

### Warning

There are a few caveats and considerations to make:

- You'll need to increase the Runners timeout to fit your wait time plus execution time
- There might be inconsistent behaviour if two pipelines wait to deploy at different times 
   - This could cause the wrong build to be deployed. 
   - Use environments to mitigate this.
